
import javax.swing.JCheckBox;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class CheckBoxExample {
    CheckBoxExample(){  
        JFrame f= new JFrame("CheckBox Example");  
        JCheckBox checkBox1 = new JCheckBox("C++");
        JCheckBox checkBox2 = new JCheckBox("Java", true);
        checkBox1.setBounds(100,100, 50,50);
        checkBox2.setBounds(100,150, 100,100); 
        f.add(checkBox1);  
        f.add(checkBox2);  
        f.setSize(400,400);  
        f.setLayout(null);  
        f.setVisible(true);
        
    }
    public static void main(String args[]){  
    new CheckBoxExample();
    }
}
